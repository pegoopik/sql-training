package controllers;

import java.util.HashMap;
import java.util.Map;

public class Security extends Secure.Security {
    public static final Map<String, String> users = new HashMap<>();

    static {
        users.put("admin", "12345");
        users.put("user", "54321");
    }

    static boolean authenticate(String username, String password) {
        //users можно получить из БД
        return users.get(username).equals(password);
    }

    static boolean check(String profile) {
        String user = connected();
        if ("administrator".equals(profile)) {
            return "admin".equals(user);
        } else {
            return false;
        }
    }

}