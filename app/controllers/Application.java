package controllers;

import play.Logger;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class Application extends Controller {

    public static void index() {
        String user = Security.connected();
        render(user);
    }

    @Check("administrator")
    public static void adminAction() {
        Logger.debug("admin action");
    }

}